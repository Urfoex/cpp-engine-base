#include <SDL.h>
#include <SDL_error.h>
#include <SDL_events.h>
#include <SDL_keycode.h>
#include <SDL_render.h>
#include <SDL_video.h>
#include <fmt/format.h>
#include <memory>
#include <stdexcept>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif // __EMSCRIPTEN__

class SDL {
public:
    struct Error : public std::runtime_error {
        Error()
        : std::runtime_error(SDL_GetError()) {}
    };

    SDL() {
        if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER
                     | SDL_INIT_GAMECONTROLLER)
            != 0) {
            throw Error();
        }
    }
    ~SDL() {
        fmt::print("SDL Quit\n");
        SDL_Quit();
    }

    class Window {
    private:
        struct Destroyer {
            auto operator()(SDL_Window * const window) -> void {
                SDL_DestroyWindow(window);
            }
        };

    public:
        std::unique_ptr<SDL_Window, Destroyer> window;
        enum Flags {
            NONE = 0,
            FULLSCREEN = SDL_WINDOW_FULLSCREEN,
            FULLSCREEN_DESKTOP = SDL_WINDOW_FULLSCREEN_DESKTOP,
            OPENGL = SDL_WINDOW_OPENGL,
            VULKAN = SDL_WINDOW_VULKAN,
            SHOWN = SDL_WINDOW_SHOWN,
            HIDDEN = SDL_WINDOW_HIDDEN,
            BORDERLESS = SDL_WINDOW_BORDERLESS,
            RESIZABLE = SDL_WINDOW_RESIZABLE,
            MINIMIZED = SDL_WINDOW_MINIMIZED,
            MAXIMIZED = SDL_WINDOW_MAXIMIZED,
            INPUT_GRABBED = SDL_WINDOW_INPUT_GRABBED,
            INPUT_FOCUS = SDL_WINDOW_INPUT_FOCUS,
            MOUSE_FOCUS = SDL_WINDOW_MOUSE_FOCUS,
            FOREIGN = SDL_WINDOW_FOREIGN,
            ALLOW_HIGHDPI = SDL_WINDOW_ALLOW_HIGHDPI,
            MOUSE_CAPTURE = SDL_WINDOW_MOUSE_CAPTURE,
            ALWAYS_ON_TOP = SDL_WINDOW_ALWAYS_ON_TOP,
            SKIP_TASKBAR = SDL_WINDOW_SKIP_TASKBAR,
            UTILITY = SDL_WINDOW_UTILITY,
            TOOLTIP = SDL_WINDOW_TOOLTIP,
            POPUP_MENU = SDL_WINDOW_POPUP_MENU,
        };
        Window(std::string_view const & title,
               int const x,
               int const y,
               int w,
               int h,
               Flags flags)
        :

            window(SDL_CreateWindow(title.data(), x, y, w, h, flags)) {
            if (this->window == nullptr) {
                throw Error();
            }
        }
    };

    class Renderer {
    private:
        struct Destroyer {
            auto operator()(SDL_Renderer * const renderer) -> void {
                SDL_DestroyRenderer(renderer);
            }
        };

    public:
        std::unique_ptr<SDL_Renderer, Destroyer> renderer;
        enum Flags {
            NONE = 0,
            SOFTWARE = SDL_RENDERER_SOFTWARE,
            ACCELERATED = SDL_RENDERER_ACCELERATED,
            PRESENTVSYNC = SDL_RENDERER_PRESENTVSYNC,
            TARGETTEXTURE = SDL_RENDERER_TARGETTEXTURE,
        };
        Renderer(SDL_Window * const window, int const index, Flags const flags)
        : renderer(SDL_CreateRenderer(window, index, flags)) {
            if (this->renderer == nullptr) {
                throw Error();
            }
        }
        auto present() const -> void {
            SDL_RenderPresent(this->renderer.get());
        }
        auto clear() const -> void {
            SDL_SetRenderDrawColor(this->renderer.get(), 0, 0, 0, 255);
            SDL_RenderClear(this->renderer.get());
        }
    };

    struct Event {
        struct {
            bool running = true;
        } data;
        SDL_Event event;
        auto poll() -> void {
            while (SDL_PollEvent(&this->event) != 0) {
                switch (this->event.type) {
                case SDL_QUIT: {
                    this->data.running = false;
                    break;
                }
                case SDL_KEYUP: {
                    switch (this->event.key.keysym.sym) {
                    case SDLK_ESCAPE: {
                        this->data.running = false;
                        break;
                    }
                    }
                    break;
                }
                case SDLK_DOWN: {
                    switch (this->event.key.keysym.sym) {
                    case SDLK_ESCAPE: {
                        break;
                    }
                    }
                    break;
                }
                default:
                    break;
                }
            }
        }
    };
};

struct Engine {
    SDL const sdl;
    SDL::Window const window;
    SDL::Renderer const renderer;
    SDL::Event event;
    Engine()
    : sdl()
    , window("C++ Engine Base",
             SDL_WINDOWPOS_CENTERED,
             SDL_WINDOWPOS_CENTERED,
             640,
             480,
             SDL::Window::Flags::SHOWN)
    , renderer(window.window.get(), -1, SDL::Renderer::Flags::ACCELERATED) {}

    auto loop() -> void {
        this->event.poll();
        this->renderer.clear();
        this->renderer.present();
    }
#ifdef __EMSCRIPTEN__
    static auto emloop(void * arg) -> void {
        auto engine = static_cast<Engine *>(arg);
        if (engine->event.data.running) {
            try {
                engine->loop();
                return;
            } catch (std::runtime_error const & error) {
                fmt::print("Error> {}\n", error.what());
            }
        }
        fmt::print("Engine::Loop - end\n");
        emscripten_cancel_main_loop();
    }
#endif
};

#ifdef __EMSCRIPTEN__
auto setUpEmscripten() -> void {
    try {
        fmt::print("Set Up Emscripten - ...\n");
        emscripten_set_main_loop([]() {}, 1, false);
        static auto engine = std::make_unique<Engine>();

        emscripten_cancel_main_loop();
        emscripten_set_main_loop_arg(
            Engine::emloop, static_cast<void *>(engine.get()), 0, true);
        fmt::print("Set Up Emscripten - Done\n");
    } catch (SDL::Error const & error) {
        fmt::print("Error: {}\n", error.what());
    }
}
#endif

auto setUp() -> void {
    try {
        fmt::print("Set Up - ...\n");
        auto engine = std::make_unique<Engine>();

        fmt::print("Running - ...\n");
        while (engine->event.data.running) {
            engine->loop();
        }
        fmt::print("Done.\n");
    } catch (SDL::Error const & error) {
        fmt::print("Error: {}\n", error.what());
    }
}

auto main() -> int {
#ifdef __EMSCRIPTEN__
    setUpEmscripten();
#else
    setUp();
#endif
    return 0;
}
