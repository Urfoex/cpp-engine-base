# C++ Engine Base

## EMSDK

### install

* git clone https://github.com/emscripten-core/emsdk.git
* ./emsdk install latest
* ./emsdk activate latest
* source ./emsdk_env.sh

### add fmt

* git clone https://github.com/fmtlib/fmt.git
* git checkout 7.1.3
* mkdir build_emsdk && cd build_emsdk
* emcmake cmake -GNinja -DFMT_TEST=false -DFMT_LIB_DIR=lib/wasm32-emscripten ..
* ninja && ninja install

## build & run with emsdk

* mkdir build_emscripten && cd build_emscripten
* emcmake cmake -GNinja -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ..
* ninja
* python3 -m http.server
* open http://0.0.0.0:8000

## build & run native

* mkdir build && cd build
* cmake -GNinja -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ..
* ninja
* ./cpp_engine_base
